﻿namespace BasicCalculator

type Calculate = 
   
   static member Divide x y = 
        match y with
        | 0 -> None
        | _ -> Some(x/y)

   static member Multiply x y = x*y
   
   static member Minus x y = x - y

   static member Add x y = x+y

   static member Mod x y = 
         match y with
        | 0 -> None
        | _ -> Some(x%y)